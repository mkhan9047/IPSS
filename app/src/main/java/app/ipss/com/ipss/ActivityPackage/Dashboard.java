package app.ipss.com.ipss.ActivityPackage;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.UiThread;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.facebook.shimmer.ShimmerFrameLayout;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import app.ipss.com.ipss.AdapterPackage.ProductRecyclerAdapter;
import app.ipss.com.ipss.HelperPackages.Utility;
import app.ipss.com.ipss.LocalStroage.Storage;
import app.ipss.com.ipss.ModelClasses.LetterList;
import app.ipss.com.ipss.ModelClasses.ProductList;
import app.ipss.com.ipss.ModelClasses.Products;
import app.ipss.com.ipss.Networking.NetworkInterface;
import app.ipss.com.ipss.Networking.RetrofitClient;
import app.ipss.com.ipss.R;
import cc.cloudist.acplibrary.ACProgressFlower;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Dashboard extends AppCompatActivity {

    RecyclerView productRecycler;
    private int count = 0;
    List<ProductList> productLists;
    ProductRecyclerAdapter adapter;
    EditText product_name;
    /*bottom sheet */
    FloatingActionButton fab;
    TextView filter_btn;
    //Spinner filter_price_range;
    Button cancel, done;
    BottomSheetDialog bottomSheetDialog;
    SwipeRefreshLayout swipeRefreshLayout;
    ImageButton log_out;

    private ShimmerFrameLayout mShimmerViewContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }


        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Dashboard.this, AddMailActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });


        InitView();




        productLists = new ArrayList<>();

        RecyclerBuilder();


        int PERMISSION_ALL = 1;
        String[] PERMISSIONS = {
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                android.Manifest.permission.CAMERA
        };

        if(!hasPermissions(this, PERMISSIONS)){
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }


        if (Utility.isInternetAvailable(this)) {
            setProductList("", "", "");
        } else {
            Utility.showNoInternetDialog(this);
        }


        filter_btn.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("DefaultLocale")
            @Override
            public void onClick(View view) {
                // toggleBottomSheet();
                View bottomView = getLayoutInflater().inflate(R.layout.bottom_sheet_layout, null);
                bottomSheetDialog = new BottomSheetDialog(Dashboard.this, R.style.SheetDialog);
                //bottomSheetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                bottomSheetDialog.setContentView(bottomView);

                final TextView tvFrom = (TextView) bottomView.findViewById(R.id.from_date);
                final TextView tvTo = (TextView) bottomView.findViewById(R.id.to_date);

                /*default date */

                tvFrom.setText(String.format("%d-%d-%d", Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH) + 1,
                        Calendar.getInstance().get(Calendar.DAY_OF_MONTH) - 7));

                tvTo.setText(String.format("%d-%d-%d", Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH) + 1,
                        Calendar.getInstance().get(Calendar.DAY_OF_MONTH)));

                /*end of default*/


                product_name = bottomView.findViewById(R.id.filter_name);
                //filter_price_range = bottomView.findViewById(R.id.filter_price_range);
                cancel = bottomView.findViewById(R.id.filter_cancel);
                done = bottomView.findViewById(R.id.filter_done);


                // Log.d("price-test", "Start " + price[0] + "\n" + "End" + price[1] );

                done.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (Utility.isInternetAvailable(Dashboard.this)) {

                            // stop animating Shimmer and hide the layout
                            mShimmerViewContainer.startShimmerAnimation();
                            mShimmerViewContainer.setVisibility(View.VISIBLE);

                            setProductList(product_name.getText().toString().trim(), tvFrom.getText().toString().trim(), tvTo.getText().toString().trim());

                        } else {

                            Utility.showNoInternetDialog(Dashboard.this);
                        }


                        if (bottomSheetDialog.isShowing()) {

                            bottomSheetDialog.dismiss();

                        }

                    }
                });


                tvTo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {


                        DatePickerDialog datePickerDialog = new DatePickerDialog(Dashboard.this, new DatePickerDialog.OnDateSetListener() {

                            @SuppressLint("DefaultLocale")
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                                //    Log.e("selectedDate", year + "/" + month + 1 + "/" + day);
                                tvTo.setText(String.format("%d-%d-%d", year, month + 1, day));
                            }
                        }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));

                        datePickerDialog.show();
                    }
                });

                tvFrom.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        DatePickerDialog datePickerDialog = new DatePickerDialog(Dashboard.this, new DatePickerDialog.OnDateSetListener() {

                            @SuppressLint("DefaultLocale")
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                                //    Log.e("selectedDate", year + "/" + month + 1 + "/" + day);
                                tvFrom.setText(String.format("%d-%d-%d", year, month + 1, day));
                            }
                        }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH) - 7);

                        datePickerDialog.show();
                    }
                });

                bottomSheetDialog.show();

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (bottomSheetDialog.isShowing()) {
                            bottomSheetDialog.dismiss();
                        }

                    }
                });
            }
        });


        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                if (Utility.isInternetAvailable(Dashboard.this)) {
                    mShimmerViewContainer.setVisibility(View.VISIBLE);
                    mShimmerViewContainer.startShimmerAnimation();
                    setProductList("", "", "");
                } else {

                    if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing()) {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                    Utility.showNoInternetDialog(Dashboard.this);
                }

            }
        });



/*
        BottomSheetDialog bottomSheetDialog = (BottomSheetDialog) dialog;
        final FrameLayout bottomSheet = (FrameLayout) ((BottomSheetDialog) dialog).findViewById(android.support.design.R.id.design_bottom_sheet);
        final BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        bottomSheetBehavior.setBottomSheetCallback(mBottomSheetBehaviorCallback);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        bottomSheetBehavior.setPeekHeight(500);
        KeyboardVisibilityEvent.setEventListener(SignInActivity.getInstance(), new KeyboardVisibilityEventListener() {
            @Override
            public void onVisibilityChanged(boolean isOpen) {
                if (!isOpen) {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
            }
        });*/


        log_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder dailog = new AlertDialog.Builder(Dashboard.this);
                dailog.setMessage(R.string.sure_to_log_out);
                dailog.setIcon(R.mipmap.ic_launcher_round);
                dailog.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        Storage storage1 = new Storage(Dashboard.this);
                        storage1.SaveLogInSate(false);
                        storage1.SaveAccessType(null);
                        storage1.SaveAccessToken(null);
                        Intent intent = new Intent(Dashboard.this, LogInActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    }
                });

                dailog.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                dailog.show();
            }
        });
    }


    private void InitView() {

        swipeRefreshLayout = findViewById(R.id.swipe);
        productRecycler = findViewById(R.id.product_recycler);
        filter_btn = findViewById(R.id.filter_btn);
        log_out = findViewById(R.id.log_out);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);


    }


    private void RecyclerBuilder() {

        productRecycler.setHasFixedSize(true);
        productRecycler.setLayoutManager(new LinearLayoutManager(this));

    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    private void setProductList(String entidade, String start_date, String endDate) {

        //  final ACProgressFlower dialog = Utility.StartProgressDialog(this, "Loading...");

        Storage storage = new Storage(this);

        NetworkInterface networkInterface = RetrofitClient.getRetrofit().create(NetworkInterface.class);

        Call<LetterList> productListCall = networkInterface.ProductList(storage.getAccessType() + " " + storage.getAccessToken(), entidade,
                start_date, endDate
        );

        productListCall.enqueue(new Callback<LetterList>() {
            @Override
            public void onResponse(Call<LetterList> call, Response<LetterList> response) {


                switch (response.code()) {
                    case 401:
                        Toast.makeText(Dashboard.this, "session expired!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(Dashboard.this, LogInActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        finish();
                        startActivity(intent);
                }

                LetterList productLists = response.body();


                if (productLists != null) {

                    Utility.CURRENT_PAGE_URL = String.valueOf(productLists.getLetterList().getCurrentPage());
                    Utility.LAST_PAGE_URL = String.valueOf(productLists.getLetterList().getLastPageUrl());
                    Utility.NEXT_PAGE_URL = String.valueOf(productLists.getLetterList().getNextPageUrl());

                    adapter = new ProductRecyclerAdapter(productLists.getLetterList().getData(), getApplicationContext());

                    productRecycler.setAdapter(adapter);

                    // stop animating Shimmer and hide the layout
                    mShimmerViewContainer.stopShimmerAnimation();
                    mShimmerViewContainer.setVisibility(View.GONE);

                    //  Utility.DismissDialog(dialog, Dashboard.this);

                    if (bottomSheetDialog != null && bottomSheetDialog.isShowing()) {
                        bottomSheetDialog.dismiss();
                    }

                    if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing()) {

                        swipeRefreshLayout.setRefreshing(false);

                    }
                }

            }

            @Override
            public void onFailure(Call<LetterList> call, Throwable t) {

                if (t instanceof SocketTimeoutException) {

                    showConnectionTimeoutDialog(Dashboard.this);

                    if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing()) {

                        swipeRefreshLayout.setRefreshing(false);

                    }
                }
                // stop animating Shimmer and hide the layout
                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
                // Utility.DismissDialog(dialog, Dashboard.this);

            }
        });

    }


    @Override
    public void onBackPressed() {
        final android.support.v7.app.AlertDialog.Builder dailog = new android.support.v7.app.AlertDialog.Builder(this);
        dailog.setMessage(R.string.sure_to_exit);
        dailog.setIcon(R.mipmap.ic_launcher_round);
        dailog.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                moveTaskToBack(true);
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);
            }
        });

        dailog.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        dailog.show();


    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        super.onPause();
    }

    public void showConnectionTimeoutDialog(Activity context) {

        final android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder(context).create();
        alertDialog.setTitle(getString(R.string.info));
        alertDialog.setMessage(context.getString(R.string.connection_timeout));
        alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.try_again), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                mShimmerViewContainer.startShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.VISIBLE);
                if (Utility.isInternetAvailable(Dashboard.this)) {
                    setProductList("", "", "");
                } else {
                    Utility.showNoInternetDialog(Dashboard.this);
                }
            }
        });


        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }
}
