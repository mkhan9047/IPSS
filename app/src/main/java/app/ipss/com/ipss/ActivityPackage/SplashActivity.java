package app.ipss.com.ipss.ActivityPackage;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import app.ipss.com.ipss.LocalStroage.Storage;
import app.ipss.com.ipss.R;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {

            Window w = getWindow();

            w.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

            w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);

        }

        final Storage storage = new Storage(this);

        /**
         * handler for showing splash screen for 4 seconds and after then finishing this activity and staring the login activity
         */
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                /**
                 * finishing this activity after 4 seconds
                 */

                finish();

                /**
                 * starting login activity after 4 seconds
                 */

                if (storage.getLogInState()) {

                    Intent intent = new Intent(SplashActivity.this, Dashboard.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();

                } else {

                    Intent intent = new Intent(SplashActivity.this, LogInActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();

                }


            }
        }, 4000);//this will finish after 4 seconds
    }


}
