package app.ipss.com.ipss.HelperPackages;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.Toast;

import app.ipss.com.ipss.R;

public class Validator {


    public static final String EMAIL_VERIFICATION = "^([\\w-\\.]+){1,64}@([\\w&&[^_]]+){2,255}.[a-z]{2,}$";


    public static boolean validateInputField(EditText[] array, Activity context) {

        int count = 0;

        for (int i = 0; i < array.length; i++) {
            if (array[i].getText().toString().length() == 0) {
                Toast.makeText(context, array[i].getTag() + " is empty!", Toast.LENGTH_SHORT).show();
            } else {
                if ((array[i].getTag().equals(context.getString(R.string.email)))) {
                    if (array[i].getText().toString().trim().matches(EMAIL_VERIFICATION)) {
                        count++;
                    } else {
                        Toast.makeText(context, array[i].getTag() + " is invalid", Toast.LENGTH_SHORT).show();
                        break;
                    }

                } else {

                    count++;
                }
            }
        }


        return array.length == count;
    }


}