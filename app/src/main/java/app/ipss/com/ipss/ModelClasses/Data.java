package app.ipss.com.ipss.ModelClasses;


import com.google.gson.annotations.SerializedName;


public class Data{

	@SerializedName("access_token")
	private String accessToken;

	@SerializedName("access_type")
	private String accessType;

	public String getAccessToken(){
		return accessToken;
	}

	public String getAccessType(){
		return accessType;
	}


}