package app.ipss.com.ipss.HelperPackages;

public class Constants {

    public static final String BASE_URL = "https://mail-management.dev01.squaredbyte.com/";
    public static final String DUMMY_IMAGE = "http://ipss.itech-theme.com/assets/back/images/media/01.jpg";
    public static final String IMAGE_FOLDER = BASE_URL + "upload/";

}
