package app.ipss.com.ipss.ModelClasses;


import com.google.gson.annotations.SerializedName;


public class CreatedAt{

	@SerializedName("date")
	private String date;

	@SerializedName("timezone")
	private String timezone;

	@SerializedName("timezone_type")
	private int timezoneType;

	public String getDate(){
		return date;
	}

	public String getTimezone(){
		return timezone;
	}

	public int getTimezoneType(){
		return timezoneType;
	}

	@Override
 	public String toString(){
		return 
			"CreatedAt{" + 
			"date = '" + date + '\'' + 
			",timezone = '" + timezone + '\'' + 
			",timezone_type = '" + timezoneType + '\'' + 
			"}";
		}
}