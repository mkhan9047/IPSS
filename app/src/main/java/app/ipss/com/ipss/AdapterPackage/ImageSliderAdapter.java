package app.ipss.com.ipss.AdapterPackage;


/*all used classes are imported here */
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import java.util.List;

import app.ipss.com.ipss.GlideApp;
import app.ipss.com.ipss.HelperPackages.Constants;

public class ImageSliderAdapter extends PagerAdapter {

    /*all global field instances and variables */
    private List<String> images;
    private LayoutInflater inflater;
    private Context context;


    /**
     * constructor for getting the array list of values and current context
     * @param images
     * @param context
     */
    public ImageSliderAdapter(List<String> images, Context context) {
        this.images = images;
        this.context = context;
    }


    /*return the total size of the list of images for setting the view pager length*/
    @Override
    public int getCount() {
        return images.size();
    }

    /**
     * return is the current view and the passed object is same or not
     * @param view
     * @param object
     * @return
     */
    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }


    /**
     * set the image view of the by getting the image form the list of images and add the imageview int the viewgroup
     * @param container
     * @param position
     * @return
     */
    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        /*make imageview with current context*/
        ImageView mImageView = new ImageView(context);
        /*set centrecrop scaling the imageview */
        mImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        /*bind the image to the imageview by taking the image from the list with position*/
       // mImageView.setImageResource(images.get(position).);
        if(images.get(position).contains("http://")){
            GlideApp.with(context).load(images.get(position))
                    .into(mImageView);
        }else{
            GlideApp.with(context).load(Constants.IMAGE_FOLDER + images.get(position))
                    .into(mImageView);
        }

        /*set imageview to the viewgroup*/
        container.addView(mImageView, 0);
        return mImageView;

    }


    /*remove the imageview if it is already there and add the new one */
    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((ImageView) object);
    }
}
