package app.ipss.com.ipss.ModelClasses;


import com.google.gson.annotations.SerializedName;


public class WalletListItem{

	@SerializedName("entidade")
	private String entidade;

	@SerializedName("foto")
	private String foto;

	@SerializedName("updated_at")
	private UpdatedAt updatedAt;

	@SerializedName("created_at")
	private CreatedAt createdAt;

	@SerializedName("deleted_at")
	private Object deletedAt;

	@SerializedName("sujeito")
	private String sujeito;

	public String getEntidade(){
		return entidade;
	}

	public String getFoto(){
		return foto;
	}

	public UpdatedAt getUpdatedAt(){
		return updatedAt;
	}

	public CreatedAt getCreatedAt(){
		return createdAt;
	}

	public Object getDeletedAt(){
		return deletedAt;
	}

	public String getSujeito(){
		return sujeito;
	}

	@Override
 	public String toString(){
		return 
			"WalletListItem{" + 
			"entidade = '" + entidade + '\'' + 
			",foto = '" + foto + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",deleted_at = '" + deletedAt + '\'' + 
			",sujeito = '" + sujeito + '\'' + 
			"}";
		}
}