package app.ipss.com.ipss.HelperPackages;

import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

import com.google.gson.Gson;

import retrofit2.Response;

public class ErrorUtils {

    public static ApiError parseError(Context context, Response<?> response) {

        ApiError error = new ApiError();

        try {

            Gson gson = new Gson();

            error = gson.fromJson(response.errorBody().charStream(), ApiError.class);

        } catch (Exception e) {

            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
        }

        if (TextUtils.isEmpty(error.getMessage())) {

            error.setMessage(response.raw().message());
        }
        return error;
    }

}

