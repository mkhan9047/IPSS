package app.ipss.com.ipss;

public interface Updater {

    void onProgressUpdate(int percentage, int finished);



    void onError();

    void onFinish();

}
