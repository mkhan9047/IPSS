package app.ipss.com.ipss.ModelClasses;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


public class Products {

    @SerializedName("wallet_list")
    private WalletList walletList;

    @SerializedName("success")
    private boolean success;

    public WalletList getWalletList() {
        return walletList;
    }

    public boolean isSuccess() {
        return success;
    }


    public class DataItem  implements Serializable{

        @SerializedName("data_de_origem")
        private String dataDeOrigem;

        @SerializedName("setor")
        private String setor;

        @SerializedName("data_de_registro")
        private String dataDeRegistro;

        @SerializedName("numero")
        private String numero;

        @SerializedName("created_at")
        private String createdAt;

        @SerializedName("numero_do_documento")
        private String numeroDoDocumento;

        @SerializedName("registro_do_usuario")
        private String registroDoUsuario;

        @SerializedName("entidade")
        private String entidade;

        @SerializedName("numero_de_verificacao")
        private String numeroDeVerificacao;

        @SerializedName("foto")
        private String foto;

        @SerializedName("updated_at")
        private String updatedAt;

        @SerializedName("departamento")
        private String departamento;

        @SerializedName("usuario_atribuido")
        private String usuarioAtribuido;

        @SerializedName("tempo_de_resposta")
        private String tempoDeResposta;

        @SerializedName("id")
        private int id;

        @SerializedName("destino")
        private String destino;

        @SerializedName("sujeito")
        private String sujeito;

        public String getDataDeOrigem() {
            return dataDeOrigem;
        }

        public String getSetor() {
            return setor;
        }

        public String getDataDeRegistro() {
            return dataDeRegistro;
        }

        public String getNumero() {
            return numero;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public String getNumeroDoDocumento() {
            return numeroDoDocumento;
        }

        public String getRegistroDoUsuario() {
            return registroDoUsuario;
        }

        public String getEntidade() {
            return entidade;
        }

        public String getNumeroDeVerificacao() {
            return numeroDeVerificacao;
        }

        public String getFoto() {
            return foto;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public String getDepartamento() {
            return departamento;
        }

        public String getUsuarioAtribuido() {
            return usuarioAtribuido;
        }

        public String getTempoDeResposta() {
            return tempoDeResposta;
        }

        public int getId() {
            return id;
        }

        public String getDestino() {
            return destino;
        }

        public String getSujeito() {
            return sujeito;
        }

        @Override
        public String toString() {
            return
                    "DataItem{" +
                            "data_de_origem = '" + dataDeOrigem + '\'' +
                            ",setor = '" + setor + '\'' +
                            ",data_de_registro = '" + dataDeRegistro + '\'' +
                            ",numero = '" + numero + '\'' +
                            ",created_at = '" + createdAt + '\'' +
                            ",numero_do_documento = '" + numeroDoDocumento + '\'' +
                            ",registro_do_usuario = '" + registroDoUsuario + '\'' +
                            ",entidade = '" + entidade + '\'' +
                            ",numero_de_verificacao = '" + numeroDeVerificacao + '\'' +
                            ",foto = '" + foto + '\'' +
                            ",updated_at = '" + updatedAt + '\'' +
                            ",departamento = '" + departamento + '\'' +
                            ",usuario_atribuido = '" + usuarioAtribuido + '\'' +
                            ",tempo_de_resposta = '" + tempoDeResposta + '\'' +
                            ",id = '" + id + '\'' +
                            ",destino = '" + destino + '\'' +
                            ",sujeito = '" + sujeito + '\'' +
                            "}";
        }
    }


    public class WalletList {

        @SerializedName("first_page_url")
        private String firstPageUrl;

        @SerializedName("path")
        private String path;

        @SerializedName("per_page")
        private int perPage;

        @SerializedName("total")
        private int total;

        @SerializedName("data")
        private List<DataItem> data;

        @SerializedName("last_page")
        private int lastPage;

        @SerializedName("last_page_url")
        private String lastPageUrl;

        @SerializedName("next_page_url")
        private String nextPageUrl;

        @SerializedName("from")
        private int from;

        @SerializedName("to")
        private int to;

        @SerializedName("prev_page_url")
        private String prevPageUrl;

        @SerializedName("current_page")
        private int currentPage;

        public String getFirstPageUrl() {
            return firstPageUrl;
        }

        public String getPath() {
            return path;
        }

        public int getPerPage() {
            return perPage;
        }

        public int getTotal() {
            return total;
        }

        public List<DataItem> getData() {
            return data;
        }

        public int getLastPage() {
            return lastPage;
        }

        public String getLastPageUrl() {
            return lastPageUrl;
        }

        public String getNextPageUrl() {
            return nextPageUrl;
        }

        public int getFrom() {
            return from;
        }

        public int getTo() {
            return to;
        }

        public String getPrevPageUrl() {
            return prevPageUrl;
        }

        public int getCurrentPage() {
            return currentPage;
        }


    }
}