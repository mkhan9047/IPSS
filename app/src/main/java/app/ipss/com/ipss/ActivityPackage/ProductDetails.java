package app.ipss.com.ipss.ActivityPackage;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.viewpagerindicator.LinePageIndicator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import app.ipss.com.ipss.AdapterPackage.ImageSliderAdapter;
import app.ipss.com.ipss.GlideApp;
import app.ipss.com.ipss.HelperPackages.Constants;
import app.ipss.com.ipss.HelperPackages.Utility;
import app.ipss.com.ipss.LocalStroage.Storage;
import app.ipss.com.ipss.ModelClasses.Department;
import app.ipss.com.ipss.ModelClasses.LetterList;
import app.ipss.com.ipss.ModelClasses.Products;
import app.ipss.com.ipss.ModelClasses.User;
import app.ipss.com.ipss.Networking.NetworkInterface;
import app.ipss.com.ipss.Networking.RetrofitClient;
import app.ipss.com.ipss.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductDetails extends AppCompatActivity {

    ViewPager pager;
    LinePageIndicator indicator;
    ImageButton back_btn;
    LetterList.DataItem items;
    TextView entidate, sujito, origin, deadline, numero_do_documento;
    TextView destino, numero_de_verifico, registro_usabrio, destination, destination_type;
    WebView description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.coordinator_product_details);


        /**
         * type casting toolbar
         */
        Toolbar toolbar = findViewById(R.id.coltool);
        /**
         * making toolbar as support action bar
         */
        setSupportActionBar(toolbar);
        /**
         * setting back button on action bar
         */
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        initView();


        items = (LetterList.DataItem) getIntent().getSerializableExtra("data");

        if (items != null) {

            // String photo = items.getFoto();

            // List<String> photosLIst = Utility.getPhotoList(photo);

            String[] photo = Utility.PhotoNameParser(items.getFoto());

            ImageSliderAdapter adapter = new ImageSliderAdapter(Arrays.asList(photo), this);

            pager.setAdapter(adapter);

            indicator.setViewPager(pager);


            /*binding other data*/
            entidate.setText(items.getEntidade());

            sujito.setText(items.getSujeito());

            numero_do_documento.setText(items.getNumeroDoDocumento());

            deadline.setText(items.getDeadline());

            origin.setText(items.getOrigin());
            //description.setText(items.getDescription());

/*
            String text;
            text = "<html><body><p align=\"justify\">";
            text += items.getDescription();
            text += "</p></body></html>";*/

          //;  description.loadData(text, "text/html", "utf-8");

            if (items.getDestinationType().equals("1")) {

                destination_type.setText("User");
                getUser();

            } else if (items.getDestinationType().equals("2")) {

                destination_type.setText("Department");
                getDepartment();

            }

            destino.setText(items.getDestino());
          /*  numero_de_verifico.setText(items.getNumeroDeVerificacao());
            registro_usabrio.setText(items.getRegistroDoUsuario());
*/
        }


    }


    private void initView() {

        pager = findViewById(R.id.image_slider);
        indicator = findViewById(R.id.indicator_pager);
        entidate = findViewById(R.id.entidade);
        sujito = findViewById(R.id.price);
       /* temp_de_respota = findViewById(R.id.tempo_de_respota);
        departmento = findViewById(R.id.department);*/
        numero_do_documento = findViewById(R.id.numeo_de_documento);
        origin = findViewById(R.id.origin);
        deadline = findViewById(R.id.deadline);
        destination_type = findViewById(R.id.destination_type);
        destination = findViewById(R.id.destination);
        destino = findViewById(R.id.destino);
      /*  numero_de_verifico = findViewById(R.id.numero_de_verificaio);
        registro_usabrio = findViewById(R.id.registro_do_usario);*/
        //  back_btn = findViewById(R.id.back_btn);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, Dashboard.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


    private void getUser() {

        Storage storage = new Storage(ProductDetails.this);
        NetworkInterface networkInterface = RetrofitClient.getRetrofit().create(NetworkInterface.class);
        Call<User> getUserList = networkInterface.getUserList(storage.getAccessType() + " " + storage.getAccessToken());

        getUserList.enqueue(new Callback<User>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<User> call, Response<User> response) {


                switch (response.code()) {
                    case 401:
                        Toast.makeText(ProductDetails.this, "session expired!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(ProductDetails.this, LogInActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                }

                User user = response.body();

                if (user != null) {

                    if (user.isSuccess()) {


                        for (User.UserListItem name : user.getUserList()) {

                            if ((name.getId() == Integer.parseInt(items.getDestination()))) {


                                if (name.getFirstName().equals(name.getLastName())) {

                                    destination.setText(name.getFirstName());

                                } else {

                                    destination.setText(name.getFirstName() + " " + name.getLastName());
                                }


                            }


                        }


                    } else {

                        Toast.makeText(ProductDetails.this, "No user found!", Toast.LENGTH_SHORT).show();
                    }


                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });

    }

    private void getDepartment() {
        Storage storage = new Storage(this);
        NetworkInterface networkInterface = RetrofitClient.getRetrofit().create(NetworkInterface.class);
        Call<Department> getUserList = networkInterface.getDepartmentList(storage.getAccessType() + " " + storage.getAccessToken());

        getUserList.enqueue(new Callback<Department>() {
            @Override
            public void onResponse(Call<Department> call, Response<Department> response) {

                switch (response.code()) {
                    case 401:
                        Toast.makeText(ProductDetails.this, "session expired!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(ProductDetails.this, LogInActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                }


                Department department = response.body();

                if (department != null) {

                    if (department.isSuccess()) {


                        for (Department.DepartmentListItem name : department.getDepartmentList()) {


                            if (name.getId() == Integer.parseInt(items.getDestination())) {


                                destination.setText(name.getTitle());

                            }


                        }


                    } else {

                        Toast.makeText(ProductDetails.this, "No user found!", Toast.LENGTH_SHORT).show();
                    }


                }
            }

            @Override
            public void onFailure(Call<Department> call, Throwable t) {

            }
        });


    }
}
