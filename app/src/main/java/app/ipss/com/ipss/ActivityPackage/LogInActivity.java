package app.ipss.com.ipss.ActivityPackage;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import java.net.SocketTimeoutException;

import javax.security.auth.login.LoginException;

import app.ipss.com.ipss.HelperPackages.Utility;
import app.ipss.com.ipss.HelperPackages.Validator;
import app.ipss.com.ipss.LocalStroage.Storage;
import app.ipss.com.ipss.ModelClasses.SignIn;
import app.ipss.com.ipss.Networking.NetworkInterface;
import app.ipss.com.ipss.Networking.RetrofitClient;
import app.ipss.com.ipss.R;
import cc.cloudist.acplibrary.ACProgressFlower;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LogInActivity extends AppCompatActivity {

    EditText email, password;
    ACProgressFlower dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.parent_login);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources()
                    .getColor(R.color.colorPrimary));
        }


        InitViews();

    }

    public void onLogIn(View view) {

        /*Log in process here*/

      /*  Intent intent = new Intent(this, Dashboard.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();*/

/*

        if (NoEmpty(TO(email)) || NoEmpty(TO(password))) {

            if (Utility.EmailValidator(TO(email))) {

                LogInCall(TO(email), TO(password));

            } else {

                Toast.makeText(this, "Invalid Email!", Toast.LENGTH_SHORT).show();

            }

        } else {

            Toast.makeText(this, "Enter every field first!", Toast.LENGTH_SHORT).show();

        }
*/


        //Validator.removeBlankSpace(email);

      /*  if (Validator.LogInValidator(email, password, this)) {
            LogInCall(email.getText().toString(), password.getText().toString());
        }*/

        email.setTag(getString(R.string.email));
        password.setTag(getString(R.string.password));
        if (Validator.validateInputField(new EditText[]{email, password}, this)) {
            if (Utility.isInternetAvailable(this)) {
                LogInCall(email.getText().toString(), password.getText().toString());
            } else {
                Utility.showNoInternetDialog(this);
            }

        }

    }

    private void InitViews() {

        email = findViewById(R.id.log_in_email);
        password = findViewById(R.id.log_in_password);

    }

    private String TO(EditText editText) {

        return editText.getText().toString().trim();

    }

    private boolean NoEmpty(String data) {

        return data.length() > 0;

    }

    private void LogInCall(String email, String Password) {

        final Storage storage = new Storage(this);

        dialog = Utility.StartProgressDialog(this, getString(R.string.loading));

        NetworkInterface networkInterface = RetrofitClient.getRetrofit().create(NetworkInterface.class);

        Call<SignIn> signInCall = networkInterface.UserSignIn(email, Password);

        signInCall.enqueue(new Callback<SignIn>() {
            @Override
            public void onResponse(Call<SignIn> call, Response<SignIn> response) {


                SignIn signIn = response.body();

                if (signIn != null) {

                    if (signIn.isSuccess()) {

                        Intent intent = new Intent(LogInActivity.this, Dashboard.class);

                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                        startActivity(intent);
                        //startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(LogInActivity.this).toBundle());

                        finish();

                        storage.SaveLogInSate(true);
                        storage.SaveAccessToken(signIn.getData().getAccessToken());
                        storage.SaveAccessType(signIn.getData().getAccessType());

                        Utility.DismissDialog(dialog, LogInActivity.this);


                    } else {


                        Utility.DismissDialog(dialog, LogInActivity.this);

                        if (signIn.getError() != null) {

                            Toast.makeText(LogInActivity.this, signIn.getError().get(0), Toast.LENGTH_SHORT).show();

                        }

                        if (signIn.getMessage() != null) {

                            Toast.makeText(LogInActivity.this, signIn.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }

                }

            }

            @Override
            public void onFailure(Call<SignIn> call, Throwable t) {

                if (t instanceof SocketTimeoutException) {
                    showConnectionTimeoutDialog();
                }
            }
        });

    }

    public void showConnectionTimeoutDialog() {

        final android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder(this).create();
        alertDialog.setTitle(getString(R.string.info));
        alertDialog.setMessage(this.getString(R.string.connection_timeout));
        alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.try_again), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


                email.setTag(getString(R.string.email));
                password.setTag(getString(R.string.password));

                if (Validator.validateInputField(new EditText[]{email, password}, LogInActivity.this)) {

                    if (Utility.isInternetAvailable(LogInActivity.this)) {

                        LogInCall(email.getText().toString(), password.getText().toString());

                    } else {
                        Utility.DismissDialog(dialog, LogInActivity.this);
                        Utility.showNoInternetDialog(LogInActivity.this);
                    }

                }


            }
        });


        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE,getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Utility.DismissDialog(dialog, LogInActivity.this);
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

}
