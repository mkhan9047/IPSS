package app.ipss.com.ipss.ModelClasses;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class Department {

    @SerializedName("department_list")
    private List<DepartmentListItem> departmentList;

    @SerializedName("success")
    private boolean success;

    public List<DepartmentListItem> getDepartmentList() {
        return departmentList;
    }

    public boolean isSuccess() {
        return success;
    }


    public class DepartmentListItem {

        @SerializedName("image")
        private String image;

        @SerializedName("updated_at")
        private String updatedAt;

        @SerializedName("description")
        private String description;

        @SerializedName("created_at")
        private String createdAt;

        @SerializedName("id")
        private int id;

        @SerializedName("title")
        private String title;

        @SerializedName("status")
        private String status;

        public String getImage() {
            return image;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public String getDescription() {
            return description;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public int getId() {
            return id;
        }

        public String getTitle() {
            return title;
        }

        public String getStatus() {
            return status;
        }
    }
}