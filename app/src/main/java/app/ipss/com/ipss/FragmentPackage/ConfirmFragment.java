package app.ipss.com.ipss.FragmentPackage;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Objects;

import app.ipss.com.ipss.ActivityPackage.AddMailActivity;
import app.ipss.com.ipss.ActivityPackage.Dashboard;
import app.ipss.com.ipss.ActivityPackage.LogInActivity;
import app.ipss.com.ipss.AdapterPackage.ImageScrollerAdapter;
import app.ipss.com.ipss.HelperPackages.Utility;
import app.ipss.com.ipss.LocalStroage.Storage;
import app.ipss.com.ipss.ModelClasses.SaveProduct;
import app.ipss.com.ipss.ModelClasses.TotalData;
import app.ipss.com.ipss.Networking.NetworkInterface;
import app.ipss.com.ipss.Networking.ProgressRequestBody;
import app.ipss.com.ipss.Networking.RetrofitClient;
import app.ipss.com.ipss.R;
import app.ipss.com.ipss.Updater;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class ConfirmFragment extends Fragment implements ProgressRequestBody.UploadCallbacks {

    ImageView taken_image;
    ProgressDialog progressDoalog;
    Button confirm_button, cancel;
    TextView entidade, sujito,  numero_do_documento;
    TextView  destino, origin, destination , deadline, destination_type;
    Updater updater;
    RecyclerView imageRecycler;
    int count = 0;

    public ConfirmFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_confirm, container, false);

        entidade = view.findViewById(R.id.entidade);
        sujito = view.findViewById(R.id.sujito);
        numero_do_documento = view.findViewById(R.id.numeo_de_documento);
       // description = view.findViewById(R.id.description);
        destino = view.findViewById(R.id.destino);
        origin = view.findViewById(R.id.origin);
     destination  = view.findViewById(R.id.destination);
        deadline = view.findViewById(R.id.deadline);
        destination_type = view.findViewById(R.id.destination_type);
        confirm_button = view.findViewById(R.id.confirm_button);
        cancel = view.findViewById(R.id.cancel);
        imageRecycler = view.findViewById(R.id.image_recycler);

        BindData();


        ImageRecyclerBuilder();

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((AddMailActivity) Objects.requireNonNull(getActivity())).FragmentTransition(new CameraFragment());
            }
        });

        confirm_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              /*  UploadDialog fragment = new UploadDialog();
                updater = fragment;
                fragment.show(Objects.requireNonNull(getActivity()).getFragmentManager(), null);*/


                Activity activity = getActivity();
                if (activity != null) {
                    if (Utility.isInternetAvailable(activity)) {
                        InitProgress();
                        UpLoadData();

                    } else {
                        Utility.showNoInternetDialog(activity);
                    }
                }


            }
        });

        return view;


    }

    private void InitProgress() {
        progressDoalog = new ProgressDialog(getActivity());
        progressDoalog.setMax(100);
        progressDoalog.setMessage("Image Uploading....");
        progressDoalog.setTitle("Upload");
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDoalog.setProgress(0);
        progressDoalog.show();
    }

    private void BindData() {

        entidade.setText(TD().getEntidate());
        sujito.setText(TD().getSujieto());
        numero_do_documento.setText(String.valueOf(TD().getNuemoroDoDocumento()));
        destino.setText(TD().getDestino());
        origin.setText(String.valueOf(TD().getOrigin()));
        deadline.setText(TD().getDeadline());
       // description.setText(TD().getDescription());
        destination_type.setText(TD().getDestination_type_name());
        destination.setText(TD().getDestination_name());

    }

    private TotalData TD() {

        return AddMailActivity.totalData;

    }


    private void UpLoadData() {

       /* final ACProgressFlower dialog = new ACProgressFlower.Builder(getActivity())
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.WHITE)
                .text("Uploading...")
                .fadeColor(Color.DKGRAY).build();
        dialog.show();*/

        Storage storage = new Storage(getActivity());
        /*request body*/
        RequestBody entidate = RequestBody.create(MultipartBody.FORM, TD().getEntidate());
        RequestBody sujieto = RequestBody.create(MultipartBody.FORM, TD().getSujieto());
        RequestBody numero_do_documento = RequestBody.create(MultipartBody.FORM, String.valueOf(TD().getNuemoroDoDocumento()));
        RequestBody destino = RequestBody.create(MultipartBody.FORM, TD().getDestino());
        RequestBody origin = RequestBody.create(MultipartBody.FORM, String.valueOf(TD().getOrigin()));
        RequestBody destination_type = RequestBody.create(MultipartBody.FORM, String.valueOf(TD().getDestination_type()));
        RequestBody destination = RequestBody.create(MultipartBody.FORM,String.valueOf(TD().getDestination()));
        RequestBody deadline = RequestBody.create(MultipartBody.FORM, TD().getDeadline());
       // RequestBody deadline =  RequestBody.create(MultipartBody.FORM, String.valueOf(TD().getDescription()));

        /*end of request body*/

        NetworkInterface networkInterface = RetrofitClient.getRetrofit().create(NetworkInterface.class);

     /*   RequestBody filePart = RequestBody.create(okhttp3.MediaType.parse("image/*"), TD().getImageFile().get(0));

        MultipartBody.Part multipart = MultipartBody.Part.createFormData("foto[]", TD().getImageFile().get(0).getName(), filePart);

        RequestBody SecondfilePart = RequestBody.create(okhttp3.MediaType.parse("image/*"), TD().getImageFile().get(0));

        MultipartBody.Part Secondmultipart = MultipartBody.Part.createFormData("foto[]", TD().getImageFile().get(0).getName(), filePart);
*/

        /*make the image array*/
        MultipartBody.Part[] imageList = new MultipartBody.Part[AddMailActivity.totalData.getImageFile().size()];
        for (int i = 1; i < AddMailActivity.totalData.getImageFile().size(); i++) {
            Log.d("file_count", String.valueOf(AddMailActivity.totalData.getImageFile().size()));
            ProgressRequestBody body = (ProgressRequestBody) new ProgressRequestBody(TD().getImageFile().get(i), this, getActivity(), TD().getImageFile().size());
            MultipartBody.Part imagePart = MultipartBody.Part.createFormData("foto[]", TD().getImageFile().get(i).getName(), body);
            imageList[i] = imagePart;
        }

        //updater.totalImage(TD().getImageFile().size());

        Call<SaveProduct> saveProductCall = networkInterface.saveProduct(storage.getAccessType() + " " + storage.getAccessToken(),
                entidate, sujieto,  numero_do_documento,
                destino, deadline,
                destination, destination_type, origin
                , imageList);


      saveProductCall.enqueue(new Callback<SaveProduct>() {
            @Override
            public void onResponse(Call<SaveProduct> call, Response<SaveProduct> response) {

                switch (response.code()) {
                    case 401:
                        Toast.makeText(getActivity(), "session expired!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getActivity(), LogInActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                }

                SaveProduct saveProduct = response.body();

                switch (response.code()) {

                    case 401:
                        Toast.makeText(getContext(), R.string.session_expired, Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getActivity(), LogInActivity.class);
                        startActivity(intent);

                        if (progressDoalog.isShowing()) {
                            progressDoalog.dismiss();
                        }

                }

                if (saveProduct != null) {

                    if (saveProduct.isSuccess()) {

                        if (progressDoalog.isShowing()) {
                            progressDoalog.dismiss();
                        }

                        AddMailActivity.totalData = null;

                        Toast.makeText(getActivity(), R.string.entry_sucessful, Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getActivity(), Dashboard.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);

                    } else {
                        for (String allError : saveProduct.getError()) {
                            Toast.makeText(getActivity(), getString(R.string.entry_unsucessful) + allError, Toast.LENGTH_SHORT).show();
                        }

                    }

                }

                if (progressDoalog.isShowing()) {
                    progressDoalog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<SaveProduct> call, Throwable t) {


                showConnectionTimeoutDialog(getActivity());

                if (progressDoalog.isShowing()) {
                    progressDoalog.dismiss();
                }
            }
        });


    }

    private void RequestBodyCreator() {


    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onProgressUpdate(final int percentage, int finshed) {

        Objects.requireNonNull(getActivity()).runOnUiThread(new Runnable() {
            @Override
            public void run() {

                progressDoalog.setProgress(percentage);
            }
        });


    }


    @Override
    public void finishedCount() {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

        count++;


        Objects.requireNonNull(getActivity()).runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (count < AddMailActivity.totalData.getImageFile().size() - 1)
                    progressDoalog.cancel();
                progressDoalog.show();
            }
        });

    }


    private void ImageRecyclerBuilder() {
        imageRecycler.setHasFixedSize(true);
        imageRecycler.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        ImageScrollerAdapter imageScrollerAdapter = new ImageScrollerAdapter(TD().getImageFile(), getActivity());
        imageRecycler.setAdapter(imageScrollerAdapter);
    }

    public void showConnectionTimeoutDialog(Activity context) {

        final android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder(context).create();
        alertDialog.setTitle(getString(R.string.info));
        alertDialog.setMessage(context.getString(R.string.connection_timeout));
        alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.try_again), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


                Activity activity = getActivity();
                if (activity != null) {
                    if (Utility.isInternetAvailable(activity)) {
                        InitProgress();
                        UpLoadData();

                    } else {
                        if (progressDoalog.isShowing() && progressDoalog != null) {
                            progressDoalog.setProgress(0);
                            progressDoalog.dismiss();
                        }
                        Utility.showNoInternetDialog(activity);
                    }
                }


            }
        });


        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }
}


