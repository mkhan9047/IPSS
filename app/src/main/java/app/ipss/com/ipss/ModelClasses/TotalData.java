package app.ipss.com.ipss.ModelClasses;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TotalData implements Serializable {

    private String entidade;
    private String sujeito;
    private int numero_do_documento;
    private String destino;
    private int Origin;
    private String deadline;
    private int destination;
    private int destination_type;
    private String destination_name;
    private String destination_type_name;



    private List<File> imageFile = new ArrayList<>();

    public TotalData(String entidate, String sujieto, String destination_name, String destination_type_name,  int destination, int destination_type, int nuemoroDoDocumento, String destino, int origin, String deadline) {
        entidade = entidate;
        sujeito = sujieto;
        this.destination_name =destination_name;
        this.destination_type_name = destination_type_name;
        this.numero_do_documento = nuemoroDoDocumento;
        this.destino = destino;
        this.Origin = origin;
        this.deadline = deadline;
        this.destination_type = destination_type;
        this.destination = destination;
    }

    public void setImageFile(File file) {

        if (imageFile.size() == 0) {
            imageFile.add(0, null);
            imageFile.add(1, file);

        } else {

            imageFile.add(file);
        }


    }

    public List<File> getImageFile() {

        return imageFile;
    }

    public String getEntidate() {
        return entidade;
    }

    public String getSujieto() {
        return sujeito;
    }

    public String getDestination_name() {
        return destination_name;
    }

    public String getDestination_type_name() {
        return destination_type_name;
    }


    public int getNuemoroDoDocumento() {

        return numero_do_documento;
    }

    public String getDestino() {

        return destino;
    }


    public int getOrigin() {
        return Origin;
    }

    public String getDeadline() {
        return deadline;
    }

    public int getDestination() {
        return destination;
    }

    public int getDestination_type() {
        return destination_type;
    }


}
