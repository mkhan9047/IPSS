package app.ipss.com.ipss.Networking;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okio.BufferedSink;

public class ProgressRequestBody extends RequestBody {

    private File mFile;
    private String mPath;
    private int finished;
    private int total_image;
    private UploadCallbacks mListener;
    Activity context;


    private static final int DEFAULT_BUFFER_SIZE = 2048;

    public interface UploadCallbacks {

        void onProgressUpdate(int percentage, int finished_Count);


        void finishedCount();

        void onError();

        void onFinish();
    }


    public ProgressRequestBody(final File file, final UploadCallbacks listener, Activity context, int total_image) {
        mFile = file;
        this.total_image = total_image;
        mListener = listener;
        this.context = context;
    }


    @Override
    public void writeTo(BufferedSink sink) throws IOException {

        final long fileLength = mFile.length();
        byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
        long uploaded = 0;

        FileInputStream in = null;

        try {

            in = new FileInputStream(mFile);


            int read;

            Handler handler = new Handler(Looper.getMainLooper());

            finished = 0;

            while ((read = in.read(buffer)) != -1) {


                uploaded += read;

                sink.write(buffer, 0, read);


                // update progress on UI thread
                // handler.postDelayed(new ProgressUpdater(uploaded, fileLength), 200);

                handler.post(new ProgressUpdater(uploaded, fileLength, finished));



            }

            mListener.onFinish();
            finished++;

        } finally {

            if (in != null) {
                in.close();
            }

        }


    }

    @Override
    public long contentLength() throws IOException {

        return mFile.length();
    }

    @Override
    public MediaType contentType() {

        return MediaType.parse("image/*");
    }


    private class ProgressUpdater implements Runnable {

        private long mUploaded;
        private long mTotal;
        private int finished;

        public ProgressUpdater(long uploaded, long total, int finished) {
            mUploaded = uploaded;
            mTotal = total;
            this.finished = finished;
        }


        @Override
        public void run() {

            mListener.onProgressUpdate((int) (100 * mUploaded / mTotal), finished);



        }
    }


}
