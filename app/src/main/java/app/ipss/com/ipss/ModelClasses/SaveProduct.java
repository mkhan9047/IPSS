package app.ipss.com.ipss.ModelClasses;

import java.util.List;

public class SaveProduct {

    private boolean success;
    private int status_code;
    private String message;
    private List<String> errors;

    public List<String> getError() {
        return errors;
    }

    public boolean isSuccess() {
        return success;
    }

    public int getStatus_code() {
        return status_code;
    }

    public String getMessage() {
        return message;
    }
}
