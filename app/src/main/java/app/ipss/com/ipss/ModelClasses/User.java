package app.ipss.com.ipss.ModelClasses;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class User{

	@SerializedName("success")
	private boolean success;

	@SerializedName("user_list")
	private List<UserListItem> userList;

	public boolean isSuccess(){
		return success;
	}

	public List<UserListItem> getUserList(){
		return userList;
	}

	public class UserListItem{

		@SerializedName("updated_at")
		private String updatedAt;

		@SerializedName("role_id")
		private String roleId;

		@SerializedName("last_name")
		private String lastName;

		@SerializedName("created_at")
		private String createdAt;

		@SerializedName("id")
		private int id;

		@SerializedName("avatar")
		private String avatar;

		@SerializedName("first_name")
		private String firstName;

		@SerializedName("email")
		private String email;

		public String getUpdatedAt(){
			return updatedAt;
		}

		public String getRoleId(){
			return roleId;
		}

		public String getLastName(){
			return lastName;
		}

		public String getCreatedAt(){
			return createdAt;
		}

		public int getId(){
			return id;
		}

		public String getAvatar(){
			return avatar;
		}

		public String getFirstName(){
			return firstName;
		}

		public String getEmail(){
			return email;
		}
	}

}