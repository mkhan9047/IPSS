package app.ipss.com.ipss.ModelClasses;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class LetterList {

    @SerializedName("success")
    private boolean success;

    @SerializedName("letter_list")
    private LetterList letterList;

    @SerializedName("first_page_url")
    private String firstPageUrl;

    @SerializedName("path")
    private String path;

    @SerializedName("per_page")
    private int perPage;

    @SerializedName("total")
    private int total;

    @SerializedName("data")
    private List<DataItem> data;

    @SerializedName("last_page")
    private int lastPage;

    @SerializedName("last_page_url")
    private String lastPageUrl;

    @SerializedName("next_page_url")
    private Object nextPageUrl;

    @SerializedName("from")
    private int from;

    @SerializedName("to")
    private int to;

    @SerializedName("prev_page_url")
    private Object prevPageUrl;

    @SerializedName("current_page")
    private int currentPage;

    public boolean isSuccess() {
        return success;
    }

    public LetterList getLetterList() {
        return letterList;
    }

    public String getFirstPageUrl() {
        return firstPageUrl;
    }

    public String getPath() {
        return path;
    }

    public int getPerPage() {
        return perPage;
    }

    public int getTotal() {
        return total;
    }

    public List<DataItem> getData() {
        return data;
    }

    public int getLastPage() {
        return lastPage;
    }

    public String getLastPageUrl() {
        return lastPageUrl;
    }

    public Object getNextPageUrl() {
        return nextPageUrl;
    }

    public int getFrom() {
        return from;
    }

    public int getTo() {
        return to;
    }

    public Object getPrevPageUrl() {
        return prevPageUrl;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    @Override
    public String toString() {
        return
                "LetterList{" +
                        "success = '" + success + '\'' +
                        ",letter_list = '" + letterList + '\'' +
                        ",first_page_url = '" + firstPageUrl + '\'' +
                        ",path = '" + path + '\'' +
                        ",per_page = '" + perPage + '\'' +
                        ",total = '" + total + '\'' +
                        ",data = '" + data + '\'' +
                        ",last_page = '" + lastPage + '\'' +
                        ",last_page_url = '" + lastPageUrl + '\'' +
                        ",next_page_url = '" + nextPageUrl + '\'' +
                        ",from = '" + from + '\'' +
                        ",to = '" + to + '\'' +
                        ",prev_page_url = '" + prevPageUrl + '\'' +
                        ",current_page = '" + currentPage + '\'' +
                        "}";
    }


    public class DataItem implements Serializable {

        @SerializedName("destination")
        private String destination;

        @SerializedName("description")
        private String description;

        @SerializedName("created_at")
        private String createdAt;

        @SerializedName("numero_do_documento")
        private String numeroDoDocumento;

        @SerializedName("registro_do_usuario")
        private String registroDoUsuario;

        @SerializedName("deadline")
        private String deadline;

        @SerializedName("origin")
        private String origin;

        @SerializedName("entidade")
        private String entidade;

        @SerializedName("numero_de_verificacao")
        private String numeroDeVerificacao;

        @SerializedName("destination_type")
        private String destinationType;

        @SerializedName("foto")
        private String foto;

        @SerializedName("updated_at")
        private String updatedAt;

        @SerializedName("id")
        private int id;

        @SerializedName("destino")
        private String destino;

        @SerializedName("sujeito")
        private String sujeito;

        @SerializedName("status")
        private String status;


        public String getDeadline() {
            return deadline;
        }

        public String getOrigin() {
            return origin;
        }

        public String getDestination() {
            return destination;
        }

        public String getDescription() {
            return description;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public String getNumeroDoDocumento() {
            return numeroDoDocumento;
        }

        public String getRegistroDoUsuario() {
            return registroDoUsuario;
        }

        public String getEntidade() {
            return entidade;
        }

        public String getNumeroDeVerificacao() {
            return numeroDeVerificacao;
        }

        public String getDestinationType() {
            return destinationType;
        }

        public String getFoto() {
            return foto;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public int getId() {
            return id;
        }

        public String getDestino() {
            return destino;
        }

        public String getSujeito() {
            return sujeito;
        }

        public String getStatus() {
            return status;
        }

        @Override
        public String toString() {
            return
                    "DataItem{" +
                            "destination = '" + destination + '\'' +
                            ",description = '" + description + '\'' +
                            ",created_at = '" + createdAt + '\'' +
                            ",numero_do_documento = '" + numeroDoDocumento + '\'' +
                            ",registro_do_usuario = '" + registroDoUsuario + '\'' +
                            ",entidade = '" + entidade + '\'' +
                            ",numero_de_verificacao = '" + numeroDeVerificacao + '\'' +
                            ",destination_type = '" + destinationType + '\'' +
                            ",foto = '" + foto + '\'' +
                            ",updated_at = '" + updatedAt + '\'' +
                            ",id = '" + id + '\'' +
                            ",destino = '" + destino + '\'' +
                            ",sujeito = '" + sujeito + '\'' +
                            ",status = '" + status + '\'' +
                            "}";
        }
    }
}