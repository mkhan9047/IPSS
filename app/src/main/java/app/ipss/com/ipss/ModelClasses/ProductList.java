package app.ipss.com.ipss.ModelClasses;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class ProductList{

	@SerializedName("wallet_list")
	public List<WalletListItem> letter_list;

	@SerializedName("success")
	private boolean success;

	public List<WalletListItem> getWalletList(){
		return letter_list;
	}

	public boolean isSuccess(){
		return success;
	}

	@Override
 	public String toString(){
		return 
			"ProductList{" + 
			"wallet_list = '" + letter_list + '\'' +
			",success = '" + success + '\'' + 
			"}";
		}
}