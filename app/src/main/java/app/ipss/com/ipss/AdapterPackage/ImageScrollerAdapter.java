package app.ipss.com.ipss.AdapterPackage;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.io.File;
import java.util.List;
import java.util.Objects;

import app.ipss.com.ipss.ActivityPackage.AddMailActivity;
import app.ipss.com.ipss.FragmentPackage.CameraFragment;
import app.ipss.com.ipss.R;

public class ImageScrollerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private List<File> imageFile;
    private Activity activity;

    public ImageScrollerAdapter(List<File> imageFile, Activity activity) {

        this.imageFile = imageFile;

        this.activity = activity;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {



        if (viewType == TYPE_HEADER) {

         View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_image_recycler_row, parent, false);

            return new HeaderHolder(view);

        } else if (viewType == TYPE_ITEM) {

            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_recycler_row, parent, false);

            return new ImageHolder(view);
        }

        return null;


    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {


        if (holder instanceof HeaderHolder) {

            HeaderHolder headerHolder = (HeaderHolder) holder;
            headerHolder.imageView.setImageResource(R.drawable.ic_photo_camera_black_24dp);

           /* holder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((AddMailActivity) Objects.requireNonNull(activity)).FragmentTransition(new CameraFragment());
                }
            });*/

        } else if (holder instanceof ImageHolder) {

            final ImageHolder imageHolder = (ImageHolder) holder;

/*    new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground( final Void ... params ) {
                    // something you know that will take a few seconds

                    return null;
                }

                @Override
                protected void onPostExecute( final Void result ) {
                    // continue what you are doing...

                }
            }.execute();*/


            new Thread(new Runnable() {
                @Override
                public void run() {

                    final Bitmap myBitmap = BitmapFactory.decodeFile(imageFile.get(position).getAbsolutePath());

                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            imageHolder.imageView.setImageBitmap(myBitmap);

                        }
                    });
                }
            }).start();



        }


    /*    if (position == 0) {


        } else {

        }*/


    }

    @Override
    public int getItemCount() {

        return imageFile.size();
    }

    @Override
    public int getItemViewType(int position) {

        if (isPositionHeader(position))

            return TYPE_HEADER;

        return TYPE_ITEM;
    }

    private File getItem(int position) {

        return imageFile.get(position - 1);
    }

    private boolean isPositionHeader(int position) {

        return position == 0;
    }

    class ImageHolder extends RecyclerView.ViewHolder {

        ImageView imageView;

        public ImageHolder(View itemView) {

            super(itemView);

            imageView = itemView.findViewById(R.id.image);
        }


    }


    class HeaderHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView imageView;

        public HeaderHolder(View itemView) {

            super(itemView);

            itemView.setOnClickListener(this);
            imageView = itemView.findViewById(R.id.header_image);
        }

        @Override
        public void onClick(View view) {

            ((AddMailActivity) Objects.requireNonNull(activity)).FragmentTransition(new CameraFragment());
        }
    }


}
