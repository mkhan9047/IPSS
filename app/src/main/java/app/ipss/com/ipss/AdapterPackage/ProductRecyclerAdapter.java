package app.ipss.com.ipss.AdapterPackage;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.facebook.shimmer.ShimmerFrameLayout;

import java.io.Serializable;
import java.net.SocketTimeoutException;
import java.util.Arrays;
import java.util.List;

import app.ipss.com.ipss.ActivityPackage.Dashboard;
import app.ipss.com.ipss.ActivityPackage.LogInActivity;
import app.ipss.com.ipss.ActivityPackage.ProductDetails;
import app.ipss.com.ipss.GlideApp;
import app.ipss.com.ipss.HelperPackages.Constants;
import app.ipss.com.ipss.HelperPackages.Utility;
import app.ipss.com.ipss.LocalStroage.Storage;
import app.ipss.com.ipss.ModelClasses.LetterList;
import app.ipss.com.ipss.ModelClasses.Products;
import app.ipss.com.ipss.Networking.NetworkInterface;
import app.ipss.com.ipss.Networking.RetrofitClient;
import app.ipss.com.ipss.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private List<LetterList.DataItem> productListsMain;
    private Context context;
    int TYPE_ITEM = 1;
    int TYPE_FOOTER = 2;

    public ProductRecyclerAdapter(List<LetterList.DataItem> productLists, Context context) {
        this.productListsMain = productLists;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_FOOTER) {
            return new FooterViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_footer, parent, false));
        } else if (viewType == TYPE_ITEM) {
            return new ProductHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.product_recycler_row, parent, false));
        }
        return null;

    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        if (viewHolder instanceof FooterViewHolder) {

            FooterViewHolder footerViewHolder = (FooterViewHolder) viewHolder;

            if (Utility.LAST_PAGE_URL.trim().equals(Utility.CURRENT_PAGE_URL.trim())) {

                footerViewHolder.progressBar.setVisibility(View.GONE);

            } else {

                if (Utility.NEXT_PAGE_URL.trim().equals("null")) {

                    // footerViewHolder.shimmerFrameLayout.setVisibility(View.GONE);
                    footerViewHolder.progressBar.setVisibility(View.GONE);

                } else {

                    if (Utility.isInternetAvailable(context)) {
                        laodNextData(Utility.NEXT_PAGE_URL);
                    } else {
                        Toast.makeText(context, R.string.cant_load_more, Toast.LENGTH_SHORT).show();
                    }


                }
            }

        } else if (viewHolder instanceof ProductHolder) {

            ProductHolder holder = (ProductHolder) viewHolder;
/*            Glide.get().load(Constants.IMAGE_FOLDER + Utility.PhotoNameParser(productListsMain.get(position).getFoto()))
                    .placeholder(R.drawable.place_holder)
                    .into(holder.productImage);*/

/*

                Glide.with(context)

                        .load(Constants.IMAGE_FOLDER + Utility.PhotoNameParser(productListsMain.get(position).getFoto()))
                        //.preload().onLoadStarted(context.getDrawable(R.drawable.place_holder))

                        .preload().onLoadStarted(context.getDrawable(R.drawable.place_holder)

                )
                        .into(holder.productImage)
*/


            String[] photo = Utility.PhotoNameParser(productListsMain.get(position).getFoto());

            if (photo[0].contains("/")) {

                GlideApp.with(context)
                        .load(photo[0])
                        .centerCrop()
                        .into(holder.productImage);
            } else {

                GlideApp.with(context)
                        .load(Constants.IMAGE_FOLDER + photo[0])
                        .centerCrop()
                        .into(holder.productImage);
            }



            holder.entidate.setText(productListsMain.get(position).getEntidade());
            holder.sujeoto.setText(productListsMain.get(position).getSujeito());
            holder.data.setText(String.format("Date: %s", Utility.ProductDateFormatter(productListsMain.get(position).getCreatedAt())));
            // Log.d("photoLInk", Constants.IMAGE_FOLDER + Utility.PhotoNameParser(productLists.get(position).getFoto()));

        }

    }

    @Override
    public int getItemCount() {

        return productListsMain.size() + 1;

    }

    private boolean isPositionFooter(int position) {
        return position == productListsMain.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionFooter(position)) {
            return TYPE_FOOTER;
        }
        return TYPE_ITEM;
    }

    class ProductHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView productImage;
        TextView entidate, sujeoto, data;


        public ProductHolder(View itemView) {

            super(itemView);
            itemView.setOnClickListener(this);
            productImage = itemView.findViewById(R.id.product_image);
            entidate = itemView.findViewById(R.id.product_entry);
            sujeoto = itemView.findViewById(R.id.product_subject);
            data = itemView.findViewById(R.id.product_date);

        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(context, ProductDetails.class);
            // intent.pustSer("image", productListsMain.get(getAdapterPosition()).getFoto());
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("data", (Serializable) productListsMain.get(getAdapterPosition()));
            context.startActivity(intent);
            //Toast.makeText(context, "cliecked", Toast.LENGTH_SHORT).show();
        }
    }


    private class FooterViewHolder extends RecyclerView.ViewHolder {
        ProgressBar progressBar;


        public FooterViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progress_bar);

        }
    }


    private void laodNextData(String url) {
        NetworkInterface networkInterface = RetrofitClient.getRetrofit().create(NetworkInterface.class);
        Storage storage = new Storage(context);
        final Call<LetterList> productListCall = networkInterface.ProductListInfineLoad(url, storage.getAccessType() + " " + storage.getAccessToken(), "",
                "", "", "", ""
        );

        productListCall.enqueue(new Callback<LetterList>() {
            @Override
            public void onResponse(Call<LetterList> call, Response<LetterList> response) {

                switch (response.code()) {
                    case 401:
                        Toast.makeText(context, "session expired!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(context, LogInActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                }

                LetterList productLists = response.body();

                if (productLists != null) {

                    Utility.CURRENT_PAGE_URL = String.valueOf(productLists.getLetterList().getCurrentPage());
                    Utility.LAST_PAGE_URL = String.valueOf(productLists.getLetterList().getLastPageUrl());
                    Utility.NEXT_PAGE_URL = String.valueOf(productLists.getLetterList().getNextPageUrl());

                    productListsMain.addAll(productLists.getLetterList().getData());

                    notifyDataSetChanged();
                }

            }

            @Override
            public void onFailure(Call<LetterList> call, Throwable t) {

                if (t instanceof SocketTimeoutException) {

                    Toast.makeText(context, R.string.connection_timeout, Toast.LENGTH_SHORT).show();

                }

            }
        });
    }

}
