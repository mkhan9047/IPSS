package app.ipss.com.ipss.FragmentPackage;


import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;


import com.google.android.cameraview.CameraView;

import java.io.File;
import java.util.Objects;

import app.ipss.com.ipss.ActivityPackage.AddMailActivity;
import app.ipss.com.ipss.HelperPackages.Utility;
import app.ipss.com.ipss.R;
import cc.cloudist.acplibrary.ACProgressFlower;


/**
 * A simple {@link Fragment} subclass.
 *
 */
public class CameraFragment extends Fragment {


    CameraView cameraView;
    FrameLayout frameLayout;
    FloatingActionButton takeImage;
    ACProgressFlower dialog;

    public CameraFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_camera, container, false);

    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();

        if (view != null) {

            cameraView = view.findViewById(R.id.camera);
            takeImage = view.findViewById(R.id.takeImage);

        }


        takeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (cameraView.isCameraOpened()) {
                    cameraView.takePicture();
                }



                /*showing progress dialog */

                dialog = Utility.StartProgressDialog(getActivity(), "Loading...");

            }
        });


        cameraView.start();

        cameraView.addCallback(new CameraView.Callback() {
            @Override
            public void onPictureTaken(CameraView cameraView,  byte[] data) {

                super.onPictureTaken(cameraView, data);


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                    cameraView.stop();

                }

               new saveImageInBackground().execute(Utility.toObjects(data));





                //cameraView.stop();


            }

            @Override
            public void onCameraClosed(CameraView cameraView) {
                super.onCameraClosed(cameraView);
            }
        });


    }



    @Override
    public void onPause() {

        cameraView.stop();

        super.onPause();
    }

    @Override
    public void onStop() {

        cameraView.stop();

        super.onStop();
    }

    @Override
    public void onDestroy() {

        cameraView.stop();

        super.onDestroy();
    }



   private class saveImageInBackground extends AsyncTask<Byte, Void, Void>{

        @Override
        protected Void doInBackground(Byte... bytes) {

            Bitmap bmp = BitmapFactory.decodeByteArray(Utility.toPrimitives(bytes), 0, bytes.length);

            Matrix matrix = new Matrix();

            matrix.postRotate(90);

            Bitmap finalBitmap = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);

            File file = Utility.saveImage(finalBitmap, getContext());


            AddMailActivity.totalData.setImageFile(file);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            Objects.requireNonNull(getActivity()).runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    Utility.DismissDialog(dialog, getActivity());

                    ((AddMailActivity) Objects.requireNonNull(getActivity())).FragmentTransition(new ConfirmFragment());

                }
            });
        }
    }
}
