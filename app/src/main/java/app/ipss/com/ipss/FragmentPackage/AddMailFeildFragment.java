package app.ipss.com.ipss.FragmentPackage;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import app.ipss.com.ipss.ActivityPackage.AddMailActivity;
import app.ipss.com.ipss.ActivityPackage.Dashboard;
import app.ipss.com.ipss.ActivityPackage.LogInActivity;
import app.ipss.com.ipss.LocalStroage.Storage;
import app.ipss.com.ipss.ModelClasses.Department;
import app.ipss.com.ipss.ModelClasses.TotalData;
import app.ipss.com.ipss.ModelClasses.User;
import app.ipss.com.ipss.Networking.NetworkInterface;
import app.ipss.com.ipss.Networking.RetrofitClient;
import app.ipss.com.ipss.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 *
 */
public class AddMailFeildFragment extends Fragment {

    Button save_button;
    EditText entidate, sujito, numero_do_documento;
    EditText destino, numero_de_verifico, registro_usabrio, origin, deadline;
    User user;
    Department department;
    Spinner des_type, destination;
    LinearLayout destination_layout;

    int destination_id;
    int destination_type;

    public AddMailFeildFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_mail_feild, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);



        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);


        View view = getView();

        if (view != null) {

            save_button = view.findViewById(R.id.save_button);
            entidate = view.findViewById(R.id.entidade);
            sujito = view.findViewById(R.id.sujito);

            numero_do_documento = view.findViewById(R.id.numeo_de_documento);
            destino = view.findViewById(R.id.destino);
            /*numero_de_verifico = view.findViewById(R.id.numero_de_verificaio);
            registro_usabrio = view.findViewById(R.id.registro_do_usario);*/
            origin = view.findViewById(R.id.origin);
            deadline = view.findViewById(R.id.deadline);

            /*spinner*/

            des_type = view.findViewById(R.id.destination_type);
            destination = view.findViewById(R.id.destination_spinner);
            destination_layout = view.findViewById(R.id.destination_layout);


        }


        des_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (i == 0) {
                    getUser();
                } else if (i == 1) {
                    getDepartment();
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        destination.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                Toast.makeText(getActivity(), String.valueOf(user.getUserList().get(i).getId()), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        save_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*need to validate both spinner*/


                if (IsEveryDataEntered()) {

                    if (des_type.getSelectedItemPosition() == 0) {
                        destination_id = user.getUserList().get(destination.getSelectedItemPosition()).getId();
                        destination_type = 1;
                    } else if (des_type.getSelectedItemPosition() == 1) {
                        destination_id = department.getDepartmentList().get(destination.getSelectedItemPosition()).getId();
                        destination_type = 2;
                    }
                    try {
                        AddMailActivity.totalData = new TotalData(EditData(entidate), EditData(sujito),

                                destination.getSelectedItem().toString(),

                                des_type.getSelectedItem().toString(),

                                destination_id, destination_type,

                                Integer.parseInt(EditData(numero_do_documento)),

                                EditData(destino),

                                Integer.parseInt(EditData(origin)),

                                EditData(deadline)

                        );


                    } catch (NumberFormatException e) {

                        Toast.makeText(getActivity(), R.string.input_vaild, Toast.LENGTH_SHORT).show();
                    }


                    int PERMISSION_ALL = 1;

                    String[] PERMISSIONS = {
                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            android.Manifest.permission.CAMERA
                    };


                    if(!hasPermissions(getActivity(), PERMISSIONS)){

                        ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, PERMISSION_ALL);

                    }else{

                        ((AddMailActivity) Objects.requireNonNull(getActivity())).FragmentTransition(new CameraFragment());
                    }



                } else {

                    Toast.makeText(getActivity(), R.string.fill_every_field, Toast.LENGTH_SHORT).show();
                }


            }


        });


        if (AddMailActivity.totalData != null) {

            BindData();

        }


    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }


    private String EditData(EditText editText) {

        return editText.getText().toString();

    }

    private String TextData(TextView textView) {
        return textView.getText().toString();
    }

    private boolean IsEveryDataEntered() {

        return EditData(entidate).length() != 0 && EditData(sujito).length() != 0
                &&
                EditData(numero_do_documento).length() != 0
                && EditData(origin).length() != 0 &&
                EditData(destino).length() != 0 && EditData(deadline).length() != 0
             ;


    }


    private void BindData() {

        entidate.setText(TD().getEntidate());
        sujito.setText(TD().getSujieto());
        numero_do_documento.setText(String.valueOf(TD().getNuemoroDoDocumento()));
        destino.setText(TD().getDestino());
        origin.setText(String.valueOf(TD().getOrigin()));
        deadline.setText(TD().getDeadline());

    }

    private TotalData TD() {

        return AddMailActivity.totalData;

    }


    private void getUser() {

        Storage storage = new Storage(getActivity());
        NetworkInterface networkInterface = RetrofitClient.getRetrofit().create(NetworkInterface.class);
        Call<User> getUserList = networkInterface.getUserList(storage.getAccessType() + " " + storage.getAccessToken());

        getUserList.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {


                switch (response.code()) {
                    case 401:
                        Toast.makeText(getActivity(), "session expired!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getActivity(), LogInActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                }

                user = response.body();

                if (user != null) {

                    if (user.isSuccess()) {

                        destination_layout.setVisibility(View.VISIBLE);

                        List<String> strings = new ArrayList<>();

                        for (User.UserListItem name : user.getUserList()) {

                            strings.add(name.getFirstName());

                        }

                        if (getActivity() != null && isAdded()) {

                            ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, strings);

                            destination.setAdapter(spinnerAdapter);
                        }

                    } else {

                        Toast.makeText(getActivity(), "No user found!", Toast.LENGTH_SHORT).show();
                    }


                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });

    }

    private void getDepartment() {
        Storage storage = new Storage(getActivity());
        NetworkInterface networkInterface = RetrofitClient.getRetrofit().create(NetworkInterface.class);
        Call<Department> getUserList = networkInterface.getDepartmentList(storage.getAccessType() + " " + storage.getAccessToken());

        getUserList.enqueue(new Callback<Department>() {
            @Override
            public void onResponse(Call<Department> call, Response<Department> response) {

                switch (response.code()) {
                    case 401:
                        Toast.makeText(getActivity(), "session expired!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getActivity(), LogInActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                }


                department = response.body();

                if (department != null) {

                    if (department.isSuccess()) {

                        destination_layout.setVisibility(View.VISIBLE);

                        List<String> strings = new ArrayList<>();

                        for (Department.DepartmentListItem name : department.getDepartmentList()) {

                            strings.add(name.getTitle());

                        }

                        if (getActivity() != null && isAdded()) {

                            ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, strings);

                            destination.setAdapter(spinnerAdapter);
                        }

                    } else {

                        Toast.makeText(getActivity(), "No user found!", Toast.LENGTH_SHORT).show();
                    }


                }
            }

            @Override
            public void onFailure(Call<Department> call, Throwable t) {

            }
        });


    }
}
