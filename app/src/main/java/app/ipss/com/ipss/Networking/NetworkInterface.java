package app.ipss.com.ipss.Networking;

import app.ipss.com.ipss.ModelClasses.Department;
import app.ipss.com.ipss.ModelClasses.LetterList;
import app.ipss.com.ipss.ModelClasses.SaveProduct;
import app.ipss.com.ipss.ModelClasses.SignIn;
import app.ipss.com.ipss.ModelClasses.User;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface NetworkInterface {

    @FormUrlEncoded
    @POST("api/signin")
    Call<SignIn> UserSignIn(@Field("email") String email, @Field("password") String password);

    @GET("api/productlist")
    Call<LetterList> ProductList(@Header("Authorization") String token, @Query("entidade") String enitdade, @Query("start_date") String start_date,
                                 @Query("end_date") String end_date
                               );

    @GET
    Call<LetterList> ProductListInfineLoad(@Url String url,@Header("Authorization") String token, @Query("entidade") String enitdade, @Query("start_date") String start_date,
                               @Query("end_date") String end_date, @Query("range_start") String range_start, @Query("range_end") String range_end
    );


    @Multipart
    @POST("api/productsave")
    Call<SaveProduct> saveProduct(@Header("Authorization") String token,
                                  @Part("entidade") RequestBody entidate,
                                  @Part("sujeito") RequestBody sujeto,
                                  @Part("numero_do_documento") RequestBody do_documento,
                                  @Part("destino") RequestBody destino,
                                  @Part("deadline") RequestBody deadline,
                                  @Part("destination_user") RequestBody destination,
                                  @Part("destination_type") RequestBody destinationType,
                                  @Part("origin") RequestBody origin,
                                  @Part MultipartBody.Part foto[]);



    @GET("api/userlist")
    Call<User> getUserList(@Header("Authorization") String token);

    @GET("api/departmentlist")
    Call<Department> getDepartmentList(@Header("Authorization") String token);
}
