package app.ipss.com.ipss.ActivityPackage;

import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import app.ipss.com.ipss.FragmentPackage.AddMailFeildFragment;
import app.ipss.com.ipss.FragmentPackage.CameraFragment;
import app.ipss.com.ipss.FragmentPackage.ConfirmFragment;
import app.ipss.com.ipss.ModelClasses.TotalData;
import app.ipss.com.ipss.R;

public class AddMailActivity extends AppCompatActivity {

    Fragment currentFragment;
    TextView roleOne, roleTwo, roleThree;
    public static TotalData totalData;
    private int count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_add_mail);


        /*type casting views*/
        InitView();

        /*default fragment*/
        FragmentTransition(new AddMailFeildFragment());

    }

    /*make the fragment transition with current fragment and the previous one, replace the fragment with new fragment */

    public void FragmentTransition(Fragment fragment) {

        currentFragment = fragment;

        IndicatorUpdater(RoleManagement(currentFragment));

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
        fragmentTransaction.replace(R.id.container_fragment, fragment);
        fragmentTransaction.commit();

    }

    private void InitView() {

        roleOne = findViewById(R.id.role_one);
        roleTwo = findViewById(R.id.role_two);
        roleThree = findViewById(R.id.role_three);

    }


    private int RoleManagement(Fragment fragment) {

        if (currentFragment instanceof AddMailFeildFragment) {

            return 0;

        } else if (currentFragment instanceof CameraFragment) {

            return 1;

        } else if (currentFragment instanceof ConfirmFragment) {

            return 2;

        }

        return 0;
    }

    private void IndicatorUpdater(int role) {

        if (role == 0) {

            /*current one*/
            roleOne.setBackgroundResource(R.drawable.indication_round_selector);
            roleOne.setTextColor(Color.WHITE);

            /*rest one*/
            roleTwo.setBackgroundResource(R.drawable.indication_round_unselector);
            roleTwo.setTextColor(Color.DKGRAY);
            roleThree.setBackgroundResource(R.drawable.indication_round_unselector);
            roleThree.setTextColor(Color.DKGRAY);

        } else if (role == 1) {
            /*current one*/
            roleTwo.setBackgroundResource(R.drawable.indication_round_selector);
            roleTwo.setTextColor(Color.WHITE);

            /*rest one*/
            roleOne.setBackgroundResource(R.drawable.indication_round_unselector);
            roleOne.setTextColor(Color.DKGRAY);
            roleThree.setBackgroundResource(R.drawable.indication_round_unselector);
            roleThree.setTextColor(Color.DKGRAY);


        } else if (role == 2) {
            /*current one*/
            roleThree.setBackgroundResource(R.drawable.indication_round_selector);
            roleThree.setTextColor(Color.WHITE);

            /*rest one*/
            roleOne.setBackgroundResource(R.drawable.indication_round_unselector);
            roleOne.setTextColor(Color.DKGRAY);
            roleTwo.setBackgroundResource(R.drawable.indication_round_unselector);
            roleTwo.setTextColor(Color.DKGRAY);
        }

    }

    @Override
    public void onBackPressed() {

        if (currentFragment instanceof ConfirmFragment) {

            FragmentTransition(new CameraFragment());

        } else if (currentFragment instanceof CameraFragment) {

            FragmentTransition(new AddMailFeildFragment());

        } else if (currentFragment instanceof AddMailFeildFragment) {

            count++;


            if (AddMailActivity.totalData == null) {

                Intent intent = new Intent(this, Dashboard.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

            } else if (count == 1 && AddMailActivity.totalData != null) {

                Toast.makeText(this, "All data will be lost! press again to continue", Toast.LENGTH_SHORT).show();
                AddMailActivity.totalData = null;

            } else if (count == 2) {

                Intent intent = new Intent(this, Dashboard.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

            }

        }

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

    }



}
