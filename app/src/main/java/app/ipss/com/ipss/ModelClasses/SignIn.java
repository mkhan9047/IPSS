package app.ipss.com.ipss.ModelClasses;


import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SignIn{

	@SerializedName("status_code")
	private int statusCode;

	@SerializedName("data")
	private Data data;

	@SerializedName("success")
	private boolean success;

	@SerializedName("message")
	private String message;

	@SerializedName("errors")
	private List<String> errors;


	public int getStatusCode(){
		return statusCode;
	}


	public List<String> getError() {
		return errors;
	}

	public Data getData(){
		return data;
	}

	public boolean isSuccess(){
		return success;
	}

	public String getMessage(){
		return message;
	}

	@Override
 	public String toString(){
		return 
			"SignIn{" + 
			"status_code = '" + statusCode + '\'' + 
			",data = '" + data + '\'' + 
			",success = '" + success + '\'' + 
			",message = '" + message + '\'' + 
			"}";
		}
}